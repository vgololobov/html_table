module HtmlTable
  class Attribute
    attr_accessor :value
    attr_reader :name
    def initialize(name, value)
      @name = name
      @value = value
    end

    def inline
      "#{name}=\"#{formatted_value}\""
    end

    def formatted_value
      return formatted_array if value.instance_of? Array
      return formatted_hash if value.instance_of? Hash

      value
    end

    def formatted_array
      value.join(' ')
    end

    def formatted_hash
      properties = ''

      value.each do |key, value|
        properties += "#{key}:#{value};"
      end

      properties
    end
  end
end
