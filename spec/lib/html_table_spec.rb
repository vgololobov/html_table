require_relative '../../lib/html_table'

describe HtmlTable do
  it 'is a Module' do
    expect(subject.class).to be_a(Module)
  end

  it 'create a table with a specified number of rows and columns' do
    rows = rand(10)
    columns = rand(10)
    html_table = subject.create(rows: rows, columns: columns)

    expected_rows = ''
    expected_columns = ''
    columns.times do
      expected_columns += '<td></td>'
    end
    rows.times do
      expected_rows += "<tr>#{expected_columns}</tr>"
    end
    expected_table = "<table>#{expected_rows}</table>"

    expect(html_table).to eq(expected_table)
  end

  it 'set the table\'s width and height' do
    width = '1000px'
    height = '1000px'
    html_table = subject.create(rows: 0, columns: 0) do
      width width
      height height
    end

    expected_table = "<table style=\"width:#{width};height:#{height};\"></table>"
    expect(html_table).to eq(expected_table)
  end

  it 'specify the table\'s border and cellpadding values' do
    rows = rand(10)
    columns = rand(10)
    border = '1px solid black'
    cellpadding = '10px'

    html_table = subject.create(rows: rows, columns: columns) do
      border border
      cellpadding cellpadding
    end

    expected_rows = ''
    expected_columns = ''
    columns.times do
      expected_columns += "<td style=\"padding:#{cellpadding};\"></td>"
    end
    rows.times do
      expected_rows += "<tr>#{expected_columns}</tr>"
    end
    expected_table = "<table style=\"border:#{border};\">#{expected_rows}</table>"

    expect(html_table).to eq(expected_table)
  end


  it 'add headers to the table' do
    header1 = 'header1'

    html_table = subject.create(rows: 1, columns: 1) do
      header(1) do
        text header1
      end
    end

    expected_table = "<table><tr><th>#{header1}</th></tr><tr><td></td></tr></table>"
    expect(html_table).to eq(expected_table)
  end

  it 'add data to the table' do
    cell_data = 'cell_data'

    html_table = subject.create(rows: 1, columns: 1) do
      row(1) do
        cell(1) do
          text cell_data
        end
      end
    end

    expected_table = "<table><tr><td>#{cell_data}</td></tr></table>"
    expect(html_table).to eq(expected_table)
  end

  it 'style the table, headers, and data cells with CSS classes' do
    table_class = 'table_class'
    header_class = 'header_class'
    data_cell_classes = 'data_cell_class1 data_cell_class2'

    html_table = subject.create(rows: 1, columns: 1) do
      classes table_class

      header(1) do
        classes header_class
      end

      row(1) do
        cell(1) do
          classes data_cell_classes
        end
      end
    end

    expected_table = "<table class=\"#{table_class}\"><tr><th class=\"#{header_class}\"></th></tr><tr><td class=\"#{data_cell_classes}\"></td></tr></table>"
    expect(html_table).to eq(expected_table)
  end
end
